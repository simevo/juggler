from datetime import datetime

from juggler.webapps.models import Instance
from scheduler import helpers


def auto_delete():
    print("loading scheduled job...")
    instances = Instance.objects.exclude(status_id="DEAD")
    for instance in instances:
        if datetime.timestamp(instance.valid_until) <= datetime.timestamp(
            datetime.now()
        ):
            helpers.delete_nginx_file(instance.id)
            helpers.delete_deployment(instance.appname)
            helpers.delete_service(instance.appname)
            instance.status = helpers.get_status("DEAD")
            instance.save()
            print(instance.appname + " has been deleted automatically")
    helpers.detect_status()
