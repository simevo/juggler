from apscheduler.schedulers.background import BackgroundScheduler

from scheduler import autodelete


def start():
    scheduler = BackgroundScheduler()
    scheduler.add_job(autodelete.auto_delete, "interval", seconds=60)
    scheduler.start()
