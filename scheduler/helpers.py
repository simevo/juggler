import os

import polling2
from django import template
from kubernetes import client, config, utils

from juggler.webapps.models import Instance, Status

if os.path.exists(os.path.expanduser("~/.kube/config")):
    config.load_kube_config()
k8s_client = client.ApiClient()
core_v1_api = client.CoreV1Api()
apps_v1_api = client.AppsV1Api()
delete_apps_v1_api = client.V1DeleteOptions()


def create_deployment(deployment):
    apps_v1_api.create_namespaced_deployment(body=deployment, namespace="default")


def delete_deployment(DEPLOYMENT_NAME):
    try:
        apps_v1_api.delete_namespaced_deployment(
            name=DEPLOYMENT_NAME,
            namespace="default",
            body=client.V1DeleteOptions(
                propagation_policy="Foreground", grace_period_seconds=5
            ),
        )
    except Exception:
        print("exception while deleting deployment")


def delete_nginx_file(instance_id):
    try:
        os.remove("webapps/" + instance_id)
    except Exception:
        print(instance_id + " file doesn't exist")


def delete_service(appname):
    try:
        core_v1_api.delete_namespaced_service(
            appname, namespace="default", body=delete_apps_v1_api
        )
    except Exception:
        print("exception while deleting service")


def create_nginx_file(instance_proxy, instance_id, minikube_ip, instance_port):
    proxy_template_1 = template.Template(instance_proxy)
    nginx_text = proxy_template_1.render(
        template.Context(
            {
                "random_app_id": instance_id,
                "minikube_ip": minikube_ip,
                "nodeport": instance_port,
            }
        )
    )
    with open("webapps/" + instance_id, "w") as f:
        f.write(nginx_text)


def deploy(yaml_file):
    utils.create_from_yaml(k8s_client, yaml_file)


def get_nodeports():
    try:
        ret = core_v1_api.list_service_for_all_namespaces(watch=False)
        return [
            x.spec.ports[0].node_port for x in ret.items if x.spec.ports[0].node_port
        ]
    except Exception:
        return []


def get_status(status_id):
    return Status.objects.all().get(id=status_id)


def detect_status():
    ret = core_v1_api.list_pod_for_all_namespaces(watch=False)
    starting_instances = Instance.objects.all().filter(status_id="STARTING")
    for starting_instance in starting_instances:
        for i in ret.items:
            if (
                i.metadata.namespace == "default"
                and i.status.phase == "Running"
                and starting_instance.id in i.metadata.name
            ):
                starting_instance.status = get_status("ALIVE")
                starting_instance.save()


@polling2.poll_decorator(check_success=lambda x: len(x) > 0, step=5, timeout=360)
def get_external_ips():
    # poll k8s API for the list of nodes and return a non-empty list of external IPs, sorted by decreasing creation time
    try:
        ret = core_v1_api.list_node(watch=False)
        nodes = {
            x.status.addresses[0].address: x.metadata.creation_timestamp
            for x in ret.items
        }
        return sorted(nodes, key=nodes.get, reverse=True)
    except Exception:
        return []
