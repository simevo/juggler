#!/bin/bash
###########
# based on: https://cyral.com/blog/how-to-auto-reload-nginx/

while true
do
 inotifywait --exclude .swp -e create -e modify -e delete -e move /etc/nginx/webapps
 if nginx -t;
 then
  echo "Detected Nginx Configuration Change"
  echo "Executing: nginx -s reload"
  nginx -s reload
 fi
done
