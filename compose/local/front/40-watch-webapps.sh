#!/bin/bash
###########
# based on: https://cyral.com/blog/how-to-auto-reload-nginx/

sh -c "/usr/local/bin/nginxReloader.sh &"
exec "$@"