all:
	yarnpkg
	cp node_modules/bootstrap/dist/css/bootstrap.min.css juggler/static/css/.
	cp node_modules/bootstrap/dist/css/bootstrap.min.css.map juggler/static/css/.
	cp node_modules/bootstrap/dist/js/bootstrap.min.js juggler/static/js/.
	cp node_modules/bootstrap/dist/js/bootstrap.min.js.map juggler/static/js/.
	cp node_modules/@fortawesome/fontawesome-free/css/fontawesome.min.css juggler/static/css/.
	cp node_modules/@fortawesome/fontawesome-free/css/brands.min.css juggler/static/css/.
	cp node_modules/@fortawesome/fontawesome-free/css/solid.min.css juggler/static/css/.
	cp node_modules/@fortawesome/fontawesome-free/webfonts/fa-brands-400.ttf juggler/static/webfonts/.
	cp node_modules/@fortawesome/fontawesome-free/webfonts/fa-brands-400.woff2 juggler/static/webfonts/.
	cp node_modules/@fortawesome/fontawesome-free/webfonts/fa-solid-900.ttf juggler/static/webfonts/.
	cp node_modules/@fortawesome/fontawesome-free/webfonts/fa-solid-900.woff2 juggler/static/webfonts/.
	cp node_modules/alpinejs/dist/cdn.min.js juggler/static/js/alpine.js
