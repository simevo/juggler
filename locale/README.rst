Translations
============

Translations will be placed in this folder when running::

    python manage.py makemessages

See: https://docs.djangoproject.com/en/4.0/topics/i18n/translation/#compiling-message-files
