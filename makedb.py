#!/usr/bin/env python3
# coding=utf-8

import os
import shutil
import uuid

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.local")
django.setup()

try:
    from juggler.webapps.models import Status, Webapp
except ImportError:
    # The above import may fail for some other reason. Ensure that the
    # issue is really that Django is missing to avoid masking other
    # exceptions on Python 2.
    raise ImportError(
        "Couldn't import Django. Are you sure it's installed and "
        "available on your PYTHONPATH environment variable? Did you "
        "forget to activate a virtual environment?"
    )


if os.path.isdir("staticfiles"):
    print("`staticfiles` directory already exist")
else:
    os.mkdir("staticfiles")
if os.path.isdir("staticfiles/media"):
    print("`staticfiles/media` directory already exist")
else:
    os.mkdir(os.path.join("staticfiles", "media"))
if os.path.isdir("staticfiles/media/icons"):
    print("`staticfiles/media/icons` directory already exist")
else:
    os.mkdir(os.path.join("staticfiles/media", "icons"))
shutil.copyfile(
    "juggler/static/images/demo-react.ico", "staticfiles/media/icons/demo-react.ico"
)
shutil.copyfile(
    "juggler/static/images/vue_app2.ico", "staticfiles/media/icons/vue_app2.ico"
)

demo_react_deployment = """apiVersion: v1
kind: Service
metadata:
  labels:
    app: {{webapp_name}}-{{webapp_id}}
  name: {{webapp_name}}-{{webapp_id}}
spec:
  ports:
    - port: 80
      targetPort: 80
      nodePort: {{nodeport}}
  selector:
    app: {{webapp_name}}-{{webapp_id}}
  type: NodePort
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: {{webapp_name}}-{{webapp_id}}
  name: {{webapp_name}}-{{webapp_id}}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: {{webapp_name}}-{{webapp_id}}
  template:
    metadata:
      labels:
        app: {{webapp_name}}-{{webapp_id}}
    spec:
      containers:
        - image: registry.gitlab.com/simevo/demo-react/demo-react:latest
          name: {{webapp_name}}-{{webapp_id}}
          ports:
            - containerPort: 80
          resources: {}
          tty: true
      restartPolicy: Always"""

demo_react_proxy = """location /{{ random_app_id }}/ {
  proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  proxy_set_header Host $host;
  proxy_pass http://{{ minikube_ip }}:{{ nodeport }}/;
  proxy_redirect default;
}"""

Webapp.objects.create(
    id=uuid.uuid4(),
    name="Demo-react",
    icon="icons/demo-react.ico",
    url="https://gitlab.com/simevo/demo-react",
    duration_hour=1,
    template=demo_react_deployment,
    public_boolean=True,
    proxy_template=demo_react_proxy,
    description="Demo react app, implements a simple full-page calculator.",
)
Webapp.objects.create(
    id=uuid.uuid4(),
    name="Demo-react-Private",
    icon="icons/demo-react.ico",
    url="https://gitlab.com/simevo/demo-react",
    duration_hour=1,
    template=demo_react_deployment,
    public_boolean=False,
    proxy_template=demo_react_proxy,
    description="Demo react app, implements a simple full-page calculator.",
)

webapp2_deployment = """apiVersion: v1
kind: Service
metadata:
  labels:
    app: {{webapp_name}}-{{webapp_id}}
  name: {{webapp_name}}-{{webapp_id}}
spec:
  ports:
    - port: 80
      targetPort: 80
      nodePort: {{nodeport}}
  selector:
    app: {{webapp_name}}-{{webapp_id}}
  type: NodePort
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: {{webapp_name}}-{{webapp_id}}
  name: {{webapp_name}}-{{webapp_id}}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: {{webapp_name}}-{{webapp_id}}
  template:
    metadata:
      labels:
        app: {{webapp_name}}-{{webapp_id}}
    spec:
      containers:
        - image: registry.gitlab.com/simevo/webapp2/frontend:latest
          name: {{webapp_name}}-{{webapp_id}}
          ports:
            - containerPort: 80
          resources: {}
          tty: true
          imagePullPolicy: Always
          env:
            - name: BASE_URL
              value: {{webapp_id}}
            - name: PUBLIC_KEY
              value: "{{public_key}}"
            - name: SERVER_ADDRESS
              value: "{{server_address}}"
      restartPolicy: Always"""

webapp2_proxy = """location /{{ random_app_id }}/ {
  proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  proxy_set_header Host $host;
  proxy_pass http://{{ minikube_ip }}:{{ nodeport }}/{{ random_app_id }}/;
  proxy_redirect default;
}"""

Webapp.objects.create(
    id=uuid.uuid4(),
    name="Webapp2",
    icon="icons/vue_app2.ico",
    url="https://gitlab.com/simevo/webapp2",
    duration_hour=1,
    template=webapp2_deployment,
    public_boolean=True,
    proxy_template=webapp2_proxy,
    description="Demo vuejs app, demonstrates table, tabs, accordion and Sankey chart.",
)
Webapp.objects.create(
    id=uuid.uuid4(),
    name="Webapp2-Private",
    icon="icons/vue_app2.ico",
    url="https://gitlab.com/simevo/webapp2",
    duration_hour=1,
    template=webapp2_deployment,
    public_boolean=False,
    proxy_template=webapp2_proxy,
    description="Demo vuejs app, demonstrates table, tabs, accordion and Sankey chart.",
)

polisim_deployment = """apiVersion: v1
kind: Service
metadata:
  labels:
    app: {{webapp_name}}-{{webapp_id}}
  name: {{webapp_name}}-{{webapp_id}}
spec:
  ports:
    - port: 80
      targetPort: 80
      nodePort: {{nodeport}}
  selector:
    app: {{webapp_name}}-{{webapp_id}}
  type: NodePort
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: {{webapp_name}}-{{webapp_id}}
  name: {{webapp_name}}-{{webapp_id}}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: {{webapp_name}}-{{webapp_id}}
  template:
    metadata:
      labels:
        app: {{webapp_name}}-{{webapp_id}}
    spec:
      containers:
        - image: gitlab.simevo.com:5050/simevo/polisim/frontend:latest
          name: {{webapp_name}}-frontend-{{webapp_id}}
          ports:
            - containerPort: 80
          resources: {}
          tty: true
          imagePullPolicy: Always
          env:
            - name: BASE_URL
              value: {{webapp_id}}
            - name: MQTT_HOST
              value: localhost
            - name: BACKEND_HOST
              value: localhost
        - image: gitlab.simevo.com:5050/simevo/polisim/backend:latest
          name: {{webapp_name}}-backend-{{webapp_id}}
          resources: {}
          tty: true
          imagePullPolicy: Always
          env:
            - name: BASE_URL
              value: {{webapp_id}}
            - name: MQTT_HOST
              value: localhost
            - name: WEBSOCKETS_PROTOCOL
              value: wss
            - name: HOSTNAME_AND_PORT
              value: saas.simevo.com
        - image: gitlab.simevo.com:5050/simevo/polisim/mqtt:latest
          name: {{webapp_name}}-mqtt-{{webapp_id}}
          resources: {}
          imagePullPolicy: Always
      imagePullSecrets:
        - name: regcred
      restartPolicy: Always"""

polisim_proxy = """location /{{ random_app_id }}/ {
  proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  proxy_set_header Host $host;
  proxy_pass http://{{ minikube_ip }}:{{ nodeport }}/{{ random_app_id }}/;
  proxy_redirect default;
}
location /{{ random_app_id }}/mqtt/ {
  proxy_pass http://{{ minikube_ip }}:{{ nodeport }}/{{ random_app_id }}/mqtt/;
  proxy_http_version 1.1;
  proxy_set_header Upgrade $http_upgrade;
  proxy_set_header Connection "upgrade";
}"""

Webapp.objects.create(
    id=uuid.uuid4(),
    name="Polisim",
    icon="icons/logo_polisim1.png",
    url="https://gitlab.com/simevo/polisim",
    duration_hour=1,
    template=polisim_deployment,
    public_boolean=False,
    proxy_template=polisim_proxy,
    description="""Educational Operator Training System (OTS) for the real-time simulation of emergency scenarios for an intensified process of treatment of VOC-containing fumes. Based on a MATLAB® model integrated in an open simulation architecture for OTS, Soft Sensors and Parallel Simulation applications.""",
)

Status.objects.create(id="STARTING", description="Instance is starting now")
Status.objects.create(id="ALIVE", description="Instance is alive")
Status.objects.create(id="DEAD", description="Instance is dead")
