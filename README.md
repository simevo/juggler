# Juggler – Manages multi-instance apps hosted on a Kubernetes cluster

**Juggler** is a server-side-rendered web-app implemented with Django 4 backed by a PostgreSQL database and the nginx web server that acts as a gatekeeper to **secondary web-apps** hosted on a Kubernetes cluster in a multi-instance fashion.

If you have:

- a [Kubernetes](https://kubernetes.io/) cluster billed on a pay-per-pod basis
- a number of web applications you want to make available in a **multi-instance** fashion (i.e. each user gets her own instance),

then you can install the **Juggler** web-app outside of the cluster (for example on an always-on virtual machine) and let it "juggle" those **secondary web-apps** up and down as required, so to minimize the costs.

For example if Juggler is installed at https://saas.example.com, the users will access their own reserved instances of the secondary web-apps at throwaway addresses such as https://saas.example.com/47edfce2-e838-46cd-8bbf-81a01e49457c.

Project home: https://gitlab.com/simevo/juggler

## Functionalities

- Authenticates users

- Lets superusers and staff users publish and manage the available secondary web-apps 

- Using the [k8s API](https://kubernetes.io/docs/concepts/overview/kubernetes-api/) via the [Python client library](https://github.com/kubernetes-client/python/):

  - it spins short-lived k8s pods on demand for each user
  - it configures the cluster to expose the matching service
  - it manages the life-cycle of these deployments

- Dynamically reconfigures nginx so that the service can be accessed from a sub-dir of the domain where Juggler itself is running.

- Forwards to the secondary web-apps an OpenID Connect (OIDC) compliant JWT (JSON Web Token) that carries information on the authenticated user which can be used to control the access.

## Project structure

- **documentation** content is in `docs/` dir (mixed [reStructuredText](https://docutils.sourceforge.io/rst.html) and [Markdown](https://daringfireball.net/projects/markdown/) formats)

- `compose/` and `.dockerignore`: **docker** setup

- `config/`, `setup.cfg` and `.envs/`: **configuration** files

- `juggler/`: where the Python source code for the apps resides

- `local.yml` and `production.yml`: **docker-compose** files for local development and deployment to production

- `makedb.py`: **helper script** to populate the DB with test secondary web-apps

- `manage.py`: the main **entry point** to start, stop and control the apps

- `requirements/`: specifies the 3rd-party Python libraries used and their versions

## Getting Started

If you want to use Juggler, read the **user docs**:

- [System Administraton Guide](docs/admin.md) to learn how to manage an instance and publish your web-apps.

- [User Guide](docs/usage.md), for the end-users

If you are a developer or sysadmin, read the **technical docs**:

- [Local Install Guide](docs/local_installation.md) to install Juggler locally

- [Deployment Guide](docs/deployment.md) to install it in production

- [Developer Guide](docs/development.md) to hack on this code.

## License

[AGPL-3.0-or-later](LICENSE)

Copyright (C) 2022-2024 [simevo s.r.l.](https://simevo.com)
