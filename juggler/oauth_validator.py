from oauth2_provider.oauth2_validators import OAuth2Validator


class CustomOAuth2Validator(OAuth2Validator):
    oidc_claim_scope = None

    def get_additional_claims(self, request):
        claims = {}
        claims["email"] = request.user.email
        claims["username"] = request.user.username
        claims["token_type"] = "access"
        groups = [group.name for group in request.user.groups.all()]
        claims["groups"] = groups
        claims["user_id"] = 1

        return claims
