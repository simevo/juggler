from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class WebappsConfig(AppConfig):
    # default_auto_field = 'django.db.models.BigAutoField'
    name = "juggler.webapps"
    verbose_name = _("Webapps")

    def ready(self):
        from scheduler import updater

        updater.start()
        try:
            import juggler.webapps.signals  # noqa F401
        except ImportError:
            pass
