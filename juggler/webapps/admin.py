# Register your models here.
from django.contrib import admin

# the module name is app_name.models
from juggler.webapps.models import Groupapp, Instance, Status, Userapp, Webapp

# Register your models to admin site, then you can add, edit, delete and search your models in Django admin site.


class MyModelAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "webapp":
            kwargs["queryset"] = Webapp.objects.filter(public_boolean=False)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


admin.site.register(Webapp)
admin.site.register(Status)
admin.site.register(Userapp, MyModelAdmin)
admin.site.register(Groupapp, MyModelAdmin)
admin.site.register(Instance)
