import os
import random
import time
import uuid
from datetime import datetime, timedelta

import pytz
import yaml
from django import template
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import Group
from django.core import serializers
from django.shortcuts import redirect, render
from django.template import Template
from django.views.generic import TemplateView
from oauth2_provider.models import Application, Grant
from oauthlib.common import generate_token

from config.settings.base import OIDC_RSA_PUBLIC_KEY
from juggler.users.models import User
from scheduler import helpers

from .models import (
    Groupapp,
    Instance,
    ReportsGroupapps,
    ReportsLiveapps,
    ReportsUserapps,
    Userapp,
    Webapp,
)


def is_valid_uuid(val):
    try:
        uuid.UUID(str(val))
        return True
    except ValueError:
        return False


class SuperUserRequiredMixin(LoginRequiredMixin, UserPassesTestMixin):
    def test_func(self):
        return self.request.user.is_superuser


class AboutPageView(TemplateView):

    template_name = "pages/about.html"

    def get(self, request):
        return render(
            request,
            "pages/about.html",
        )


class IndexPageView(TemplateView):

    template_name = "pages/home.html"

    def get(self, request):
        public_app_list = Webapp.objects.filter(public_boolean=True)
        reserved_app_list = []
        reserved_userapps = Userapp.objects.filter(user_id=request.user.id)
        for reserved_userapp in reserved_userapps:
            webapp = Webapp.objects.get(id=reserved_userapp.webapp_id)
            if not webapp.public_boolean:
                reserved_app_list.append(webapp)
        groups = [group for group in request.user.groups.all()]
        for group in groups:
            reserved_groupapps = Groupapp.objects.filter(group_id=group.id)
            for reserved_groupapp in reserved_groupapps:
                webapp = Webapp.objects.get(id=reserved_groupapp.webapp_id)
                if not webapp.public_boolean:
                    reserved_app_list.append(webapp)

        instance_list = Instance.objects.filter(user_id=request.user.id).exclude(
            status_id="DEAD"
        )
        data = {
            "public_app_list": public_app_list,
            "reserved_app_list": reserved_app_list,
            "instance_list": instance_list,
        }
        return render(request, "pages/home.html", data)


class StartView(TemplateView):

    template_name = "pages/start.html"

    def get(self, request, pk):
        if not request.user.is_authenticated:
            return redirect("/accounts/login/?next=%s" % request.path)

        app_id = pk
        if not is_valid_uuid(app_id):
            return redirect("/")
        if not Webapp.objects.filter(id=app_id).exists():
            return redirect("/")

        requested_app = Webapp.objects.all().get(id=app_id)
        if not requested_app.public_boolean:
            reserved_userapp = Userapp.objects.filter(
                user_id=request.user.id, webapp_id=app_id
            )
            if not reserved_userapp.exists():
                reserved_app_list = []
                groups = [group for group in request.user.groups.all()]
                for group in groups:
                    reserved_groupapp = Groupapp.objects.filter(
                        group_id=group.id, webapp_id=app_id
                    )
                    if reserved_groupapp.exists():
                        reserved_app_list.append(reserved_groupapp)
                if len(reserved_app_list) == 0:
                    return redirect("/")

        app_template = requested_app.template
        proxy_template = requested_app.proxy_template
        user_id = request.user.id

        # choose a random node port which is not yet used
        instances = Instance.objects.exclude(status_id="DEAD")
        my_nodeports = [x.nodeport for x in instances]
        k8s_nodeports = helpers.get_nodeports()
        nodeports = my_nodeports + k8s_nodeports
        port = random.randrange(30000, 32768)
        while port in nodeports:
            port = random.randrange(30000, 32768)
        nodeport = str(port)

        random_app_id = str(uuid.uuid4())

        app_template1 = Template(app_template)
        # with protocol but without port and trailing slash
        server_address = (
            "https://" + os.getenv("DJANGO_ALLOWED_HOSTS")
            if os.getenv("DJANGO_ALLOWED_HOSTS")
            else "http://localhost"
        )
        result = app_template1.render(
            template.Context(
                {
                    "webapp_name": requested_app.name.lower(),
                    "webapp_id": random_app_id,
                    "nodeport": nodeport,
                    "public_key": OIDC_RSA_PUBLIC_KEY,
                    "server_address": server_address,
                }
            )
        )
        user_webapp_name = requested_app.name.lower() + "-" + random_app_id
        path = "juggler/media/deployments"
        if os.path.isdir(path):
            yaml_file = path + "/deployment" + random_app_id + ".yaml"
        else:
            os.makedirs(path)
            yaml_file = path + "/deployment" + random_app_id + ".yaml"
        with open(yaml_file, "w") as fout:
            fout.write(result)

        helpers.deploy(yaml_file)
        ips = helpers.get_external_ips()
        ip = ips[0]
        helpers.create_nginx_file(proxy_template, random_app_id, ip, nodeport)

        # os.remove(yaml_file)

        url = "/" + random_app_id + "/"

        user = User.objects.all().get(id=user_id)

        curr_time = datetime.utcnow().replace(tzinfo=pytz.utc)

        valid_time = requested_app.duration_hour
        valid_until = curr_time + timedelta(hours=valid_time)

        status_starting = helpers.get_status("STARTING")
        Instance.objects.create(
            id=random_app_id,
            user=user,
            webapp=requested_app,
            status=status_starting,
            valid_until=valid_until,
            ip=ip,
            nodeport=nodeport,
            appname=user_webapp_name,
        )
        curr_application = Application(
            name=user_webapp_name,
            user=user,
            client_id=random_app_id,
            authorization_grant_type=Application.GRANT_AUTHORIZATION_CODE,
            client_type=Application.CLIENT_PUBLIC,
            skip_authorization=True,
            redirect_uris=url,
            algorithm=Application.RS256_ALGORITHM,
        )
        client_secret = curr_application.client_secret
        curr_application.save()
        tok = generate_token()
        Grant.objects.create(
            code=tok,
            user=user,
            application=curr_application,
            redirect_uri=url,
            claims={},
            expires=curr_time + timedelta(hours=10),
            scope="openid",
        )
        data = {
            "webapp_name": requested_app.name,
            "url": url,
            "app_id": random_app_id,
            "client_id": curr_application.client_id,
            "client_secret": client_secret,
            "code": tok,
        }

        return render(request, "pages/start.html", data)


class DeleteView(TemplateView):
    template_name = "pages/home.html"

    def get(self, request, pk):
        app_id = pk
        requested_app = Instance.objects.all().get(id=app_id)
        data = {"pk": requested_app.id}
        requested_app.status = helpers.get_status("DEAD")
        helpers.delete_deployment(requested_app.appname)
        time.sleep(5)
        requested_app.save()
        return render(request, "pages/delete_view.html", data)


class CompletelyDeleteView(TemplateView):
    template_name = "pages/home.html"

    def get(self, request, pk):
        app_id = pk
        requested_app = Instance.objects.all().get(id=app_id)
        data = {"id": requested_app.id}
        requested_app.delete()
        helpers.delete_service(requested_app.appname)
        helpers.delete_nginx_file(app_id)
        return render(request, "pages/completely_delete_view.html", data)


class RestoreView(TemplateView):
    template_name = "pages/home.html"

    def get(self, request, pk):
        app_id = pk
        requested_app = Instance.objects.all().get(id=app_id)
        requested_app.status = helpers.get_status("ALIVE")
        requested_app.save()
        app_template = Webapp.objects.all().get(id=requested_app.webapp_id).template
        app_name = Webapp.objects.all().get(id=requested_app.webapp_id).name.lower()
        url = "/" + app_id + "/"
        app_template_1 = app_template.split("---")[1]
        app_template1 = Template(app_template_1)
        # with protocol but without port and trailing slash
        server_address = (
            "https://" + os.getenv("DJANGO_ALLOWED_HOSTS")
            if os.getenv("DJANGO_ALLOWED_HOSTS")
            else "http://localhost"
        )
        result = app_template1.render(
            template.Context(
                {
                    "webapp_name": app_name,
                    "webapp_id": app_id,
                    "public_key": OIDC_RSA_PUBLIC_KEY,
                    "server_address": server_address,
                }
            )
        )
        print(result)
        result_1 = yaml.load(result, Loader=yaml.FullLoader)
        helpers.create_deployment(result_1)
        data = {"id": requested_app.id, "appname": requested_app.appname, "url": url}
        return render(request, "pages/restore.html", data)


def rearrange(array, allowed_fields):
    rearranged = {}
    for item in array:
        rearranged[item["pk"]] = {
            k: v for k, v in item["fields"].items() if k in allowed_fields
        }
    return rearranged


class ReportsPageView(SuperUserRequiredMixin, TemplateView):

    template_name = "pages/reports.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        users = User.objects.filter(is_active=True)
        context["users"] = rearrange(
            serializers.serialize("python", users), ["username", "email"]
        )
        groups = Group.objects.all()
        context["groups"] = rearrange(serializers.serialize("python", groups), ["name"])
        webapps = Webapp.objects.all()
        context["webapps"] = rearrange(
            serializers.serialize("python", webapps),
            ["name", "url", "description", "public_boolean"],
        )
        reports_userapps = ReportsUserapps.objects.all()
        context["reports_userapps"] = serializers.serialize("python", reports_userapps)
        reports_liveapps = ReportsLiveapps.objects.all()
        context["reports_liveapps"] = serializers.serialize("python", reports_liveapps)
        reports_groupapps = ReportsGroupapps.objects.all()
        context["reports_groupapps"] = serializers.serialize(
            "python", reports_groupapps
        )

        return context
