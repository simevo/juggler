from django.urls import path

from juggler.webapps.views import IndexPageView

app_name = "webapps"
urlpatterns = [
    path("", view=IndexPageView, name="index"),
]
