from modeltranslation.translator import TranslationOptions, translator

from .models import Webapp


class WebappTranslationOptions(TranslationOptions):
    fields = ["description"]


translator.register(Webapp, WebappTranslationOptions)
