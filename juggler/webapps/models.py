import uuid

from django.contrib.auth.models import Group
from django.db import models

from juggler.users.models import User


class Webapp(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=20)
    icon = models.FileField(upload_to="icons/")
    url = models.CharField(max_length=100)
    duration_hour = models.IntegerField()
    template = models.TextField()
    public_boolean = models.BooleanField()
    proxy_template = models.TextField()
    user_apps = models.ManyToManyField(User, through="Instance")
    description = models.TextField()
    ctime = models.DateTimeField(auto_now_add=True, null=True)
    mtime = models.DateTimeField(auto_now=True)
    # this function will be invoked when this model object is foreign key of other model(for example Employee model.).

    def __str__(self):
        ret = self.name
        return ret

    class Meta:
        unique_together = ["name"]


class Userapp(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    webapp = models.ForeignKey(Webapp, on_delete=models.CASCADE)
    ctime = models.DateTimeField(auto_now_add=True, null=True)
    mtime = models.DateTimeField(auto_now=True)

    def __str__(self):
        ret = self.user.name + "<->" + self.webapp.name
        return ret

    class Meta:
        unique_together = ["user", "webapp"]


class Groupapp(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    webapp = models.ForeignKey(Webapp, on_delete=models.CASCADE)
    ctime = models.DateTimeField(auto_now_add=True, null=True)
    mtime = models.DateTimeField(auto_now=True)

    def __str__(self):
        ret = self.group.name + "<->" + self.webapp.name
        return ret

    class Meta:
        unique_together = ["group", "webapp"]


class Status(models.Model):
    id = models.CharField(max_length=20, primary_key=True)
    description = models.TextField()

    def __str__(self):
        ret = self.id
        return ret


class Instance(models.Model):
    id = models.CharField(primary_key=True, max_length=100)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    webapp = models.ForeignKey(Webapp, on_delete=models.CASCADE)
    status = models.ForeignKey(Status, on_delete=models.CASCADE)
    valid_until = models.DateTimeField()
    ip = models.CharField(max_length=20)
    nodeport = models.IntegerField()
    appname = models.TextField(max_length=100)
    ctime = models.DateTimeField(auto_now_add=True, null=True)
    mtime = models.DateTimeField(auto_now=True)


class ReportsUserapps(models.Model):
    id = models.TextField(primary_key=True)
    webapp = models.ForeignKey(Webapp, on_delete=models.DO_NOTHING)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    count = models.BigIntegerField(blank=True, null=True)
    total_lifetime = models.DurationField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "webapps_reports_userapps"


class ReportsLiveapps(models.Model):
    id = models.TextField(primary_key=True)
    webapp = models.ForeignKey(Webapp, on_delete=models.DO_NOTHING)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    age = models.DurationField(blank=True, null=True)
    status = models.ForeignKey(Status, on_delete=models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = "webapps_reports_liveapps"


class ReportsGroupapps(models.Model):
    id = models.TextField(primary_key=True)
    webapp = models.ForeignKey(Webapp, on_delete=models.DO_NOTHING)
    group = models.ForeignKey(Group, on_delete=models.DO_NOTHING)
    count = models.BigIntegerField(blank=True, null=True)
    total_lifetime = models.DurationField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "webapps_reports_groupapps"
