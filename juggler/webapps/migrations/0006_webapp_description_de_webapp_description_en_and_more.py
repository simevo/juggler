# Generated by Django 4.2.7 on 2024-01-17 09:32

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("webapps", "0005_reportsgroupapps_reportsliveapps_reportsuserapps"),
    ]

    operations = [
        migrations.AddField(
            model_name="webapp",
            name="description_de",
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name="webapp",
            name="description_en",
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name="webapp",
            name="description_fr",
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name="webapp",
            name="description_it",
            field=models.TextField(null=True),
        ),
    ]
