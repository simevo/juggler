from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("webapps", "0003_mtime_ctime"),
    ]

    operations = [
        migrations.RunSQL(
            sql="""
                CREATE VIEW webapps_reports_userapps AS
                    SELECT
                        webapp_id::TEXT || '_' || user_id::TEXT AS id,
                        webapp_id,
                        user_id,
                        COUNT(*),
                        SUM(mtime - ctime) AS total_lifetime
                    FROM webapps_instance
                    WHERE status_id = 'DEAD'
                    GROUP BY webapp_id, user_id;
                CREATE VIEW webapps_reports_liveapps AS
                    SELECT
                        webapp_id::TEXT || '_' || user_id::TEXT AS id,
                        webapp_id,
                        user_id,
                        NOW() - ctime AS age,
                        status_id
                    FROM webapps_instance
                    WHERE status_id != 'DEAD'
                    ORDER BY ctime;
                CREATE VIEW webapps_reports_groupapps AS
                    SELECT
                        webapp_id::TEXT || '_' || users_user_groups.group_id::TEXT AS id,
                        webapp_id,
                        users_user_groups.group_id,
                        COUNT(*),
                        SUM(webapps_instance.mtime - webapps_instance.ctime) AS total_lifetime
                    FROM
                      webapps_instance
                      JOIN users_user_groups ON webapps_instance.user_id = users_user_groups.user_id
                    WHERE status_id = 'DEAD'
                    GROUP BY webapp_id, users_user_groups.group_id;
            """,
            reverse_sql="""
                DROP VIEW webapps_reports_userapps;
                DROP VIEW webapps_reports_liveapps;
                DROP VIEW webapps_reports_groupapps;
            """,
        )
    ]
