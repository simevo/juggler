from django.contrib.sites.models import Site
from django.shortcuts import get_object_or_404

from config import version

site = get_object_or_404(Site, pk=1)


def add_variables_to_context(request):
    return {
        "site_name": site.name,
        "site_domain": site.domain,
        "version": version.__version__,
    }
