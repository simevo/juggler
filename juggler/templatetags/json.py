import json

from django import template

register = template.Library()


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


@register.filter
def loads(json_string):
    return json.loads(json_string)
