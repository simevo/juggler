from uuid import UUID

from allauth.account.views import LoginView
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views.generic import DetailView, RedirectView, UpdateView

from juggler.webapps.models import Webapp

User = get_user_model()


def is_valid_uuid(val):
    try:
        UUID(str(val))
        return True
    except ValueError:
        return False


class UserDetailView(LoginRequiredMixin, DetailView):

    model = User
    slug_field = "username"
    slug_url_kwarg = "username"


user_detail_view = UserDetailView.as_view()


class UserUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):

    model = User
    fields = ["name"]
    success_message = _("Information successfully updated")

    def get_success_url(self):
        assert (
            self.request.user.is_authenticated
        )  # for mypy to know that the user is authenticated
        return self.request.user.get_absolute_url()

    def get_object(self):
        return self.request.user


user_update_view = UserUpdateView.as_view()


class UserRedirectView(LoginRequiredMixin, RedirectView):

    permanent = False

    def get_redirect_url(self):
        # return reverse("users:detail", kwargs={"username": self.request.user.username})
        return reverse("home")


user_redirect_view = UserRedirectView.as_view()


class JugglerLoginView(LoginView):
    def get_context_data(self, **kwargs):
        ret = super(JugglerLoginView, self).get_context_data(**kwargs)
        next = self.request.GET.get("next")
        if next and next.startswith("/start/"):
            uuid = next[7:]
            if is_valid_uuid(uuid):
                ret.update(
                    {
                        "webapp": Webapp.objects.get(pk=uuid),
                    }
                )
        return ret
