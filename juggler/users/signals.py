import requests
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver

from juggler.users.models import User


def get_url(path):
    return (
        settings.PIPEDRIVE_BASE_URL
        + path
        + "?api_token="
        + settings.PIPEDRIVE_API_TOKEN
    )


def post_person_to_pipedrive(name, email):
    url = get_url("/persons")
    data = {"name": name, "email": email, "label": 15}
    response = requests.post(url, data=data)
    data = response.json()
    if data["success"]:
        return data["data"]
    else:
        print("error posting person to pipedrive")
        print("status code = ", response.status_code)
        print("error message: ", data["error"])
        return None


@receiver(post_save, sender=User)
def user_saved(sender, instance, created, **kwargs):
    if created:
        name = instance.username
        email = instance.email
        post_person_to_pipedrive(name, email)
