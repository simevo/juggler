from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path
from django.views import defaults as default_views

from juggler.users.views import JugglerLoginView
from juggler.webapps.views import (
    AboutPageView,
    CompletelyDeleteView,
    DeleteView,
    IndexPageView,
    ReportsPageView,
    RestoreView,
    StartView,
)

urlpatterns = [
    path("", IndexPageView.as_view(template_name="pages/home.html"), name="home"),
    path("o/", include("oauth2_provider.urls", namespace="oauth2_provider")),
    path(
        "start/<str:pk>",
        StartView.as_view(template_name="pages/start.html"),
        name="start_view",
    ),
    path(
        "delete/<str:pk>",
        DeleteView.as_view(template_name="pages/delete_view.html"),
        name="delete_view",
    ),
    path(
        "completely_delete/<str:pk>",
        CompletelyDeleteView.as_view(template_name="pages/completely_delete_view.html"),
        name="completely_delete_view",
    ),
    path(
        "restore/<str:pk>",
        RestoreView.as_view(template_name="pages/restore.html"),
        name="restore",
    ),
    path(
        "about/", AboutPageView.as_view(template_name="pages/about.html"), name="about"
    ),
    path("reports/", ReportsPageView.as_view(), name="reports"),
    # Django Admin, use {% url 'admin:index' %}
    path(settings.ADMIN_URL, admin.site.urls),
    # User management
    path("users/", include("juggler.users.urls", namespace="users")),
    path("accounts/login/", JugglerLoginView.as_view(), name="juggler_login"),
    path("accounts/", include("allauth.urls")),
    # Your stuff: custom urls includes go here
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
if settings.DEBUG:
    # Static file serving when using Gunicorn + Uvicorn for local web socket development
    urlpatterns += staticfiles_urlpatterns()


if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
    ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
