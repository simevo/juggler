.. Juggler documentation master file, created by
   sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Juggler's documentation!
======================================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   howto
   users
   development.md
   usage.md
   deployment.md



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
