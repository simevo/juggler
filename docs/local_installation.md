# Juggler Local Install Guide

This guide details how to install Juggler locally for development purposes.

[[ TOC ]]

## Preprequisites

### Docker

Install docker and docker-compose:

- on macOS 10.14 and later: install Docker Desktop for Mac from https://docker.com (make sure Docker Desktop is running !)

- on Debian 11 (bullseye) or later:

    ```sh
    sudo apt install docker.io docker-compose
    ```

    then read `/usr/share/doc/docker.io/README.Debian` !

- on Fedora 34/35, see: https://docs.docker.com/engine/install/fedora/

- on Windows (untested): install Docker Desktop for Windows (with Linux containers) from: https://hub.docker.com/editions/community/docker-ce-desktop-windows/; then enable WSL 2 integration: https://docs.docker.com/docker-for-windows/wsl/.

### Minikube

To run Juggler locally, you need a test kubernetes cluster to play with; **minikube** is highly recommended for this purpose, and this guide assumes you want to use that.

With the minikube set-up all pods live "inside" the minikube container so all apps will be available from the IP of the minikube container, each on a different port.

Now set up [the Kubernetes tools](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/) and a local minikube [minikube](https://minikube.sigs.k8s.io/docs/start/) instance on your host computer, using the docker driver (`minikube start --driver=docker`).

Now edit `~/.kube/config` to have relative references for `~/.minikube` in `clusters.cluster.certificate-authority`, `users.user.client-certificate` and `users.user.client-key` keys (i.e. instead of `/home/<username>/.minikube/profiles/minikube/client.key`, set `../.minikube/profiles/minikube/client.key` etc.) (this has to be repeated each time minikube is restarted). This command makes these changes:

    sed -i "s/${HOME////\\/}/../g" "$HOME/.kube/config"

After these config changes, verify that you can still connect to the k8s cluster with: `kubectl get po -A`.

## App secrets

Create a `secrets` directory, it should contain:

- `oidc.key`: the private key for signing the OpenID Connect tokens (generate with `openssl genrsa -out secrets/oidc.key 4096`)

- `passwd.client`: the exim4 smarthost credentials file that will end up in `/etc/exim4/passwd.client`

## Start Juggler

To start Juggler locally, run:

    docker-compose -f local.yml up

this command builds the images if required; to manualy force the rebuild (if dependencies have changed), launch:

    docker-compose -f local.yml build

**NOTE**: fron time to time, refresh the base images with: `docker pull python:3.11.6-slim-bullseye && docker pull nginx:1.25.3 && docker pull postgres:15 && docker pull adminer` and rebuild.

## Set up your users

- To create a **normal user account**, just go to Sign Up and fill out the form. Once you submit it, you'll see a "Verify Your E-mail Address" page. Go to your console to see a simulated email verification message. Copy the link into your browser. Now the user's email should be verified and ready to go.

- To create a **superuser account**, use this command [inside the django service](https://docs.docker.com/compose/reference/exec/):

        docker-compose -f local.yml run --rm django python manage.py createsuperuser --email root@example.com --username root

For convenience, you can keep your normal user logged in on Chrome and your superuser logged in on Firefox (or similar), so that you can see how the site behaves for both kinds of users.

## Connect Juggler to minikube

Connect the docker containers where the **Juggler backend** and **nginx** are running with the minikube cluster using the command:

    docker network connect minikube juggler_local_django
    docker network connect minikube juggler_local_front

## Populate the database

To populate the database, run this command:

    docker-compose -f local.yml run --rm django python makedb.py

after running `docker-compose -f local.yml up`.

Now create new secondary web-apps or edit the existing ones in the Django admin (log in with the superuser account created before): [http://localhost:8000/admin/webapps/webapp/](http://localhost:8000/admin/webapps/webapp/).

When you create the apps, you can set them as public (everyone sees and can start them) or as private (only users who are allowed can see and start them).

## Trouble-shooting

### Django fails to start

If the django service fails to start with this error in the `juggler_local_django` logs:

    kubernetes.config.config_exception.ConfigException: File does not exist: /home/paolog/.minikube/ca.crt

double-check that`~/.kube/config` does not contain absolute paths only valid on the host.

### Connection to the server localhost:8080 refused

If you get the error `The connection to the server localhost:8080 was refused - did you specify the right host or port?` it means that Juggler is not aware of your kube config.

Do not use `sudo` to start `docker-compose`.

Make sure you run `docker-compose` with the same user that installed and started minikube. Otherwise you need to create symlinks from `/home/<minikubeuser>/.kube`  and `/home/<dockercomposeuser>/.minikube`  to  `/home/<minikubeuser>/.kube` and  `/home/<dockercomposeuser>/.minikube`:

    sudo rm -rf /home/<dockercomposeuser>/.kube
    sudo ln -s /home/<minikubeuser>/.kube /root
    sudo rm -rf /home/<dockercomposeuser>/.minikube
    sudo ln -s /home/<minikubeuser>/.minikube /home/<dockercomposeuser>

Check that the django service sees the kube config:

    docker-compose -f local.yml run --rm django ls -la /root/.kube

### Deploy does not work

If the deploy fails with the message "_An error occurred._" and this error in the `juggler_local_django` logs:

    Failed to establish a new connection: [Errno 110] Connection timed out')': /api/v1/namespaces/default/services

it means the `django` service can not reach the minikube instance. Verify the network connection between Juggler and minikube cluster using the commands:

    docker-compose -f local.yml exec django bash
    apt update
    apt install iputils-ping
    ping 192.168.49.2

When you click Deploy button the django container executes load_kube_config(), for that to work we share the host `.kube` and `.minikube` dirs from the host computer into the container; you can troubleshoot that part by installing `kubectl` inside the django container, see https://gitlab.com/simevo/juggler/-/blob/main/docs/usage.md#connect-juggler-to-minikube (optional part). 

Go through the Hello Minikube tutorial (https://kubernetes.io/docs/tutorials/hello-minikube/) and make sure it works in the host computer.

Verify that you can connect to the k8s cluster from within the docker container where the Juggler backend is running with:

    docker-compose -f local.yml exec django bash
    apt update
    apt install curl
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    chmod u+x kubectl
    mv kubectl /usr/bin/.
    kubectl get po -A
