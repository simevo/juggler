# Juggler Developer Guide

This guide is for those who want to hack on Juggler. Before you start, install Juggler locally as per the [Local Install Guide](./local_installation.md).

[[ TOC ]]

## Settings

See the [Cookiecutter Django settings reference](http://cookiecutter-django.readthedocs.io/en/latest/settings.html)

## Basic Commands

### Starting the web-app

To start the web-app in development mode, run:

    docker-compose -f local.yml up

Now you can:

- Open the web-app: [http://localhost:8000](http://localhost:8000)

- Open the Django administration Site (use superuser account credentials, see below): [http://localhost:8000/admin/](http://localhost:8000/admin/)

- Open the project documentation: [http://localhost:9000/](http://localhost:9000/)

- Browse the database schema and tables with [Adminer](https://www.adminer.org/): [http://localhost:8090](http://localhost:8090/?pgsql=postgres&username=KKwSIxdDqCvoiQSrDfXGOqIBApCddGfr&db=juggler&ns=public) (use credentials from `.envs/.local/.postgres`)

- Access the PostgreSQL database with `PGPASSWORD=1jhwmhpM192HgVkdcC9r4oqe1uPhc2NJOrsQdQtThxEe1byN0hFd2XGluDBk9JoO psql -p 5433 -h localhost -U KKwSIxdDqCvoiQSrDfXGOqIBApCddGfr juggler`.

### Type checks

Running type checks with mypy:

    docker-compose -f local.yml run --rm django mypy juggler

### Test coverage

To run the tests, check your test coverage, and generate an HTML coverage report:

    docker-compose -f local.yml run --rm django coverage run -m pytest --ignore=ansible
    docker-compose -f local.yml run --rm django coverage html
    open htmlcov/index.html

#### Running tests with pytest

    docker-compose -f local.yml run --rm django pytest --ignore=ansible

### Live reloading and Sass CSS compilation

See [Live reloading and SASS compilation](http://cookiecutter-django.readthedocs.io/en/latest/live-reloading-and-sass-compilation.html).

## Updating JavaScript dependencies

JS and CSS dependencies are managed with `yarnpkg` and the files are put in place with `make` then checked into the repo.

In this way the files are vendored and the repo is self-sufficient, but it's easy to upgrade them and add new dependencies.

To **upgrade** the dependencies:

    yarnpkg upgrade

then reinstall them:

    make

To **add a new dependency**, install it with `yarnpkg add ...` or `yarnpkg add -D ...`, update the `Makefile` to put in place the files you need and `git add ...` them.

Dont't forget to commit your changes!

### Translations

Create a `.po` message file for a new language in `locales/xx/LC_MESSAGES/` with:

    docker-compose -f local.yml run --rm django python manage.py makemessages -l xx

After modifying the templates, update the existing `.po` files with:

    docker-compose -f local.yml run --rm django python manage.py makemessages -a

Convert the `.po` files into `.mo` files with:

    docker-compose -f local.yml run --rm django python manage.py compilemessages

For more details see the [Django translations docs](https://docs.djangoproject.com/en/4.0/topics/i18n/translation/).
