# Juggler Deployment Guide

This document details how to deploy Juggler to production.

[[ TOC ]]

## Preprequisites

**Sandbox** and **production** installs are preferably done with [ansible](https://www.ansible.com/), minimum required version is 2.10.

Ansible must be called with the [COLLECTIONS_PATH](https://docs.ansible.com/ansible/latest/reference_appendices/config.html#collections-paths) option set the directory where the collections are located (rather than in the default location `~/.ansible`).

Install it on the control host:

- Debian 11 (bullseye) or later: `sudo apt install ansible`

- macOS:

 - `brew install ansible`
 
 - alternate [using pip](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-macos)

Then set-up a dedicated Debian 11 (bullseye) machine as a lxc / nspawn container, VirtualBox Virtual Machine (VM), cloud-hosted Virtual Private Server (VPS) or baremetal machine; for local tests and development, an lxc container is OK.

Assuming the dedicated machine can be reached as `192.168.1.100`, make sure your `/etc/hosts` has something like:

```
192.168.1.100   saas.simevo.com
```

Set up passwordless ssh access for user `root` to the target, then install ansibile prerequisites:

    ssh root@saas.simevo.com apt update
    ssh root@saas.simevo.com apt install -y python3

## Install juggler

Launch the install with:

    ANSIBLE_COLLECTIONS_PATH="$PWD/ansible" ansible-playbook -i ansible/hosts ansible/sandbox.yml

## Kubernetes cluster

To run Juggler in production, you need access to a real kubernetes cluster (ideally billed on a pay-per-pod fashion) or for a small deployment, a single-host [k3s](https://k3s.io/) cluster.

You'll need to:

- on the Juggler host, install `kubectl` and configure access to the kubernetes cluster for the `juggler` user

- upload credentials required to pull images from your private registry, for example:

      # cp ~/.docker/config.json docker_config.json
      # edit docker_config.json to only keep the required auth key
      kubectl create secret generic regcred --from-file=.dockerconfigjson=docker_config.json --type=kubernetes.io/dockerconfigjson

- open the port range 30000-32767, for example [for GKC you need to use the command](https://cloud.google.com/kubernetes-engine/docs/how-to/exposing-apps):

      gcloud compute firewall-rules create test-node-port --project my-project-112233 --allow tcp:30000-32767

With the real kubernetes cluster the pods hop between the nodes of the cluster, but the `NodePort` service makes them available from the external IP, each on a different port.

### DB sharing

Ths easiest way to share DB between secondary webapp instances is to install Postgres on the k3s host itself, using a multi-cluster config to separate the DBs as required. 

**NOTE**: you'll still need to properly configure the webapps to access the shared DB!

Log in to the k3s host, and find out the control plane IP address with `ip addr` (look for the `cni0` interface); in the following we'll assume it's `10.42.0.1`.

Install Postgres, delete the default cluster then create a cluster for your app (example: `mywebapp`):

```sh
sudo apt install postgresql
pg_lsclusters
sudo pg_dropcluster 13 main --stop
sudo pg_createcluster -p 12121 13 mywebapp
sudo pg_ctlcluster 13 mywebapp start
pg_lsclusters
```

To make the DB accessible from the pods, we'll now configure `Endpoint` and `Service` for it.

Apply this deployment with the command `kubectl apply -f postgres-mywebapp-server-endpoint.yaml`:

```yaml
apiVersion: v1
kind: Endpoints
metadata:
  name: postgres-mywebapp-server
subsets:
  - addresses:
      - ip: 10.42.0.1
    ports:
      - port: 12121
```

Apply this deployment with the command `kubectl apply -f postgres-mywebapp-service.yaml`:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: postgres-mywebapp-server
spec:
  ports:
    - port: 12121
      targetPort: 12121
```

Configure Postgres to listen to the control plane IP address where the service is listening (`kubectl get svc postgres-mywebapp-server`) by editing the `/etc/postgresql/13/mywebapp/postgresql.conf` file so that this line is present:

```
listen_addresses = 'localhost,10.43.33.25'
```

or better yet a range:
```
listen_addresses = 'localhost,10.43.0.0/16'
```

Configure Postgres to allow connections coming from the pods by editing the `/etc/postgresql/13/mywebapp/pg_hba.conf` file so that this line is present:

```
host    all             all             10.42.0.0/24            md5
host    all             all             10.43.0.0/24            md5
```

finally restart Postgres:

```sh
sudo systemctl restart postgresql
```

Test the connection as follows:

```sh
kubectl run -it --rm --restart=Never debian --image=debian:bookworm-slim bash
apt update && apt install -y postgresql-client iproute2 iputils-ping nmap
ping postgres-webapp-server
nmap -Pn postgres-webapp-server
# replace password, username and database with your actual values
PGPASSWORD=password psql -U username -h postgres-webapp-server -p 12121 database
```

In the webapp template, use a connection string similar to: `postgres://username:password@postgres-webapp-server:12121/database`.

## Manual steps

Create the `saas.simevo.com:/srv/juggler/.env` file:

    DJANGO_SETTINGS_MODULE=config.settings.production
    DJANGO_SECRET_KEY=aaaaaaaaaaaaaaaaaaaaa
    DJANGO_ALLOWED_HOSTS=saas.simevo.com
    DATABASE_URL=postgres://juggler:juggler@127.0.0.1:5432/juggler
    REDIS_URL=redis://127.0.0.1:6379/1
    DJANGO_ADMIN_URL=admin/
    DJANGO_ACCOUNT_ALLOW_REGISTRATION=True

For getting the verification email if the MTA fails:

    cat /var/log/exim4/mainlog
    cat /var/spool/exim4/input/1nZK6q-000K7J-3t-D

Manually populate the DB:

    scp makedb.py root@saas.simevo.com:/srv/juggler/.
    ./makedb.py

Other stuff:

    hostnamectl set-hostname saas.simevo.com
    cat /etc/hosts
    127.0.0.1 localhost 
    127.0.1.1 saas.simevo.com
    ...
    hostname --fqdn
    systemctl restart exim4

    chown juggler:juggler juggler/media/deployments/
    mkdir /srv/juggler/webapps
    chown juggler:juggler /srv/juggler/webapps
    ln -s /srv/juggler/webapps /etc/nginx/webapps

    mkdir /srv/juggler/staticfiles
    chown juggler:juggler /srv/juggler/staticfiles
    python manage.py collectstatic --noinput
    python manage.py migrate

Install `gcloud` tools as per https://cloud.google.com/sdk/docs/install#deb.

Configure k8s as per https://console.cloud.google.com/kubernetes/config.

# Email

Test the SMTP configuration:

    echo "My message" | mail -s subject paolo.greppi@libpf.com

Check what exim is doing:

    systemctl status exim4
    exiwhat

Count the messages in the queue:

    exim -bpc

List outbound emails in exim4 queue:

    mailq

Force reprocessing of the quete and retry sending all messages (verbose mode):

    exim4 -qff -v

View e-mail headers for a specific message in the queue:

    exim -Mvh <message_id>

View e-mail body for a specific message in the queue:

    exim -Mvb <message_id>

Remove one or more messages from the queue:

    exim -Mrm <message_id_1> <message_id_2> <...>

Clear all frozen messages from the queue:

    mailq | awk '/frozen/{print "exim4 -Mrm "$3}' | /bin/sh

## Galaxy dependencies

Ansible galaxy is configured with a [YAML role requirements file](https://docs.ansible.com/ansible/latest/user_guide/collections_using.html), the necessary collections have been installed with:

    ANSIBLE_COLLECTIONS_PATH="$PWD/ansible" ansible-galaxy install -r ansible/requirements.yml

and are checked into the repository. To force re-download from galaxy [while we wait for the ansible-galaxy update option](https://github.com/ansible/proposals/issues/23):

    rm -rf ansible/collections
    ANSIBLE_COLLECTIONS_PATH="$PWD/ansible" ansible-galaxy install --force -r ansible/requirements.yml
