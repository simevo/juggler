# Juggler User Guide

The Juggler app acts as a gatekeeper to controlled access web applications.

Log in to Juggler as a normal user at [http://localhost:8000](http://localhost:8000) (for local development) or [https://saas.example.com](https://saas.example.com) (for production).

You'll be shown the list of web applications that you have deployed; this list is initially empty:

![screen-shot of Juggler showing an empty list of deployed apps](/juggler/static/images/juggler1.png)

Clicking on the "_Deploy a new app_" button opens a modal window with the list of web applications that you can deploy (some apps may be reserved, these are marked with the lock icon):

![screen-shot of Juggler showing the available apps that can be deployed](/juggler/static/images/juggler2.png)

If you click "_Deploy_" for one of these apps, Juggler starts showing a spinner and does some preparation in the background as the app is started in (this should take less than one minute, please be patient!):

![screen-shot of Juggler showing an app being deployed with rotating spinner icon](/juggler/static/images/juggler3.png)

After a while, the spinner disappears and the browser changes location to something like `https://saas.example.com/47edfce2-e838-46cd-8bbf-81a01e49457c` (example address), where you can use your own reserved instance of the chosen app. If the app allows it, you can also freely share links to your instance with your colleagues.

Now if you get back to the Juggler home page, the new app is shown in the list of deployed web applications, and can be reopened at will:

![screen-shot of Juggler showing the list of deployed apps](/juggler/static/images/juggler4.png)

Each user can deploy several apps and even multiple instances of the same app; each user gets an independent instance of each app she deploys ("_multi-instance Software-as-a-Service_").

The deployed apps will be available for a predefined amount of time, after that they will be automatically deleted. If you try to open a deployed app after that, the link will return "_502 Bad Gateway_". Luckily you can start from scratch and deploy a new one!
